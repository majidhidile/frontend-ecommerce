import React from "react";
import { Images } from "../Utility/Images";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";

export  function SearchArea() {
  const navigate=useNavigate()
  const cartItems = useSelector((state) => state.cartList.cartItems);
  return (
    <section className='search-container'>
      <div className="cursor" onClick={()=>navigate('/')}>
        <img style={{ height: 40, objectFit: "contain" }} src={Images.Logo2} />
      </div>
      <div className='center-horiztl'>
        <div className='clasifle-container'>
          <div>Classifieds</div>
          <div className='center-horiztl'>
            <img className="arrow-img" src={Images.DownArrow} />
          </div>
        </div>
      </div>

      <div className='search-cont'>
        <div className='search-inp-container'>
          <div><input type="text" className="search-inp" placeholder="Search Here ..."/></div>
          <div>
            <img className="arrow-img" src={Images.Search} />
          </div>
        </div>
      </div>

      <div className='center-container'>
        <div style={{ display: "flex",justifyContent:'space-evenly' }}>
          <div>
            <img  className="srch-img" src={Images.Fav} />
          </div>
          <div style={{position:'relative'}} className="cursor" onClick={()=>navigate('/cart')}>
            { !!cartItems?.length &&<div className="cart-count">{cartItems?.length}</div>}
            <img className="srch-img" src={Images.Cart} />
          </div>
          <div>
            <img className="srch-img" src={Images.Profile} />
          </div>
        </div>
      </div>

      <div className='clasified-container'>
        <div className="clsy-btn">Classifieds</div>
      </div>
    </section>
  );
}
