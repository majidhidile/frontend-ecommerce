import React from "react";
import { Images } from "../Utility/Images";

export function Category() {
  const category = [
    "Books",
    "Electronics",
    "real estate",
    "Cars-bikes",
    "Dorm-furniture",
    "Men",
    "Women",
    "Music",
    "hobbies Games",
    "toys",
    "Kids",
  ];
  return (
    <div className='catgory-container'>
      <div style={{ display: "flex", gap: 10 }}>
        <div>All Categories</div>
        <div className='center-horiztl'>
          <img style={{ height: 15, width: 15 }} src={Images.DownArrow} />
        </div>
      </div>
      <div style={{ display: "flex", gap: 20 }}>
        {category.map((name) => {
          return <div className='font-cat'>{name}</div>;
        })}
      </div>
    </div>
  );
}
