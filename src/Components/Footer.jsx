import React from "react";
import { Images } from "../Utility/Images";

export default function Footer() {
  return (
    <div className='footer-container'>
      {/* LOGO CONTAINER */}
      <div>
        <div>
          <img
            src={Images.Logo2}
            className="footer-logo"
          />
        </div>
        <div className='footer-font'>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore{" "}
        </div>
      </div>
      {/* END LOGO CONTAINER */}
      {/* QICK LICKS CONTAINER */}
      <div>
        <div className="footer-head">QUICK LINKS</div>
        <div className="footer-sub-font">Products</div>
        <div  className="footer-sub-font">Classifieds</div>
        <div  className="footer-sub-font">Contact us</div>
        <div  className="footer-sub-font">Login</div>
        <div  className="footer-sub-font">Sign Up</div>
      </div>
      {/* END QICK LICKS CONTAINER */}

      {/* CUSTOMER AREA CONTAINER */}
      <div>
        <div className="footer-head">CUSTOMER AREA</div>
        <div className="footer-sub-font">My Account</div>
        <div  className="footer-sub-font">Orders</div>
        <div  className="footer-sub-font">Tracking List</div>
        <div  className="footer-sub-font">Terms</div>
        <div  className="footer-sub-font">Privacy Policy</div>
        <div  className="footer-sub-font">Return policy</div>
        <div  className="footer-sub-font">My Cart</div>
      </div>
      {/* END CUSTOMER AREA CONTAINER */}


      {/* VENDOR AREA CONTAINER */}
      <div>
        <div className="footer-head">VENDOR AREA</div>
        <div className="footer-sub-font">Partner with us</div>
        <div  className="footer-sub-font">Training</div>
        <div  className="footer-sub-font">Procedures</div>
        <div  className="footer-sub-font">Terms</div>
        <div  className="footer-sub-font">Privacy Policy</div>
      </div>
      {/* ENDVENDOR AREA CONTAINER */}
      {/* CONTACT CONTAINER */}
      <div>
      <div className="footer-head">CUSTOMER AREA</div>
      <div className='footer-font'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut</div>
      <div className="footer-ques-contain">
        <div style={{flex:1}}>
            <img src={Images.Headphone} className="head-img"  />
        </div>
        <div style={{flex:4}}>
            <div className="question">Have any question?</div>
            <div className="number">+ 123 456 789</div>
        </div>
        <div className="chat-continer">
            <div className="chat">CHAT</div>
        </div>
      </div>
      <div className="store-container">
        <div style={{flex:1}}>
           <img src={Images.GoogleStore} style={{height:50}} />
        </div>
        <div style={{flex:1}} >
           <img src={Images.AppStore} style={{height:50}} />

        </div>
      </div>
      </div>

      {/* END CONTACT CONTAINER */}

    </div>
  );
}
