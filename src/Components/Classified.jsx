import React, { useRef } from "react";
import { Images } from "../Utility/Images";

export function Classified() {
  let data = [
    {
      name: "Holy Stone HS470 Foldable GPS RC Drone",
      img: Images.Dron,
      price: "$300.98",
    },
    {
      name: "Marshall Stockwell II Portable Speaker",
      img: Images.Marshal,
      price: "$300.98",
    },
    {
      name: "2020 Ford Mustang GT Fastback GT Fastback",
      img: Images.Car,
      price: "$22,822",
    },
    {
      name: "Holy Stone HS470 Foldable GPS RC Drone",
      img: Images.Dron,
      price: "$300.98",
    },
    {
      name: "Marshall Stockwell II Portable Speaker",
      img: Images.Marshal,
      price: "$300.98",
    },
    {
      name: "2020 Ford Mustang GT Fastback GT Fastback " ,
      img: Images.Car,
      price: "$22,822",
    },
    {
      name: "Holy Stone HS470 Foldable GPS RC Drone",
      img: Images.Dron,
      price: "$300.98",
    },
    {
      name: "Marshall Stockwell II Portable Speaker",
      img: Images.Marshal,
      price: "$300.98",
    },
    {
      name: "2020 Ford Mustang GT Fastback GT Fastback ",
      img: Images.Car,
      price: "$22,822",
    },
    {
      name: "Holy Stone HS470 Foldable GPS RC Drone",
      img: Images.Dron,
      price: "$300.98",
    },
    {
      name: "Marshall Stockwell II Portable Speaker",
      img: Images.Marshal,
      price: "$300.98",
    },
    {
      name: "2020 Ford Mustang GT Fastback GT Fastback ",
      img: Images.Car,
      price: "$22,822",
    },
    {
      name: "Holy Stone HS470 Foldable GPS RC Drone",
      img: Images.Dron,
      price: "$300.98",
    },
    {
      name: "Marshall Stockwell II Portable Speaker",
      img: Images.Marshal,
      price: "$300.98",
    },
    {
      name: "2020 Ford Mustang GT Fastback GT Fastback ",
      img: Images.Car,
      price: "$22,822",
    },
  ];
  const scrollContainerRef = useRef(null);

  const scrollLeft = () => {
    scrollContainerRef.current.scrollBy({
      left: -252, // Adjust the scroll distance as needed
      behavior: "smooth", // Optional: Add smooth scrolling effect
    });
  };

  const scrollRight = () => {
    scrollContainerRef.current.scrollBy({
      left: 252, // Adjust the scroll distance as needed
      behavior: "smooth", // Optional: Add smooth scrolling effect
    });
  };

  return (
    <section className='clasy-container'>
      <div className='clasy-sub-con'>
        <div className='clasyAll-contain'>
          <div className='clasy-font'>Classified Products on the week</div>
          <div className='arow-container'>
            <div className='arrw-con' onClick={() => scrollLeft()}>
              <img src={Images.Less} className="btn-img" />
            </div>
            <div className='arrw-con' onClick={() => scrollRight()}>
              <img src={Images.Great} className="btn-img"/>
            </div>
          </div>
          <div className='hrzdl-align' style={{ marginTop: 25 }}>
            <div className='explr-btn'>
              <div className='explor-font'>Explore</div>
              <div>
                <img src={Images.Arrow} style={{ height: 15, width: 15 }} />
              </div>
            </div>
          </div>
        </div>
        <div className='explor-scroll' ref={scrollContainerRef}>
          <div style={{ display: "flex", gap: 25 }}>
            {data.map((res) => {
              return (
                <div className='card-container'>
                  <div className='card-first'>
                    <img className='card-img' src={res.img} />
                  </div>
                  <div style={{padding: '11px 7px'}}>
                    <div className='card-name'>{res.name}</div>
                    <div className="price-container">
                      <div className="card-price">{res.price}</div>
                      <div style={{ display: "flex", gap: 3 }}>
                        <div className="img-cntr">
                          <img
                            src={Images.Locationn}
                            style={{ height: 10, width: 10 }}
                          />
                        </div>
                        <div className="card-locatin">Cape Hadstone</div>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </section>
  );
}
