import React from 'react'
import { Images } from '../Utility/Images'

export default function Subscribe() {
  return (
    <div className='subscribe-container'>
        <div className='img-container'>
            <img src={Images.Subscription} className='sub-image' />
        </div>
        <div className='newletter-container'>
            <div className='sub-sign-txt'>Sign Up for Newsletter</div>
            <div className='sub-sign-lorm'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
        </div>
        <div className='subtxt-container' >
        <div className='sub-card-container'>
            <div style={{flex:3,display:"flex"}}>
                <input className='sub-input' type='text' placeholder='Enter your email here..' />
            </div>
            <div className='subscribe-txt' >SUBSCRIBE</div>
        </div>
        </div>

    </div>
  )
}
