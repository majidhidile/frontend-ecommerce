import React from "react";
import { Images } from "../Utility/Images";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

export function BestDeals() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const productList = useSelector((state) => state.productList.products);

  const handleAddToCart = (product) => {
    // Dispatch action to add the product to the cart
    dispatch({ type: "ADD_TO_CART", payload: product });
    // Dispatch action to reduce the count in the product list
    dispatch({ type: "DECREMENT_COUNT", payload: product.id });
    navigate('/cart')
  };

  return (
    <section className='bestdeal-container'>
      <div className='best-con'>
        <div className='best-font'>Best Deals</div>
        <div className='all-font'>View all</div>
      </div>

      <div className='deal-container'>
        {productList.map((item) => {
          return (
            <div style={{ display: "flex", gap: 15 }}>
              <div style={{ width: "40%" }}>
                <img
                  src={item.img}
                  style={{
                    objectFit: "contain",
                    height: "100%",
                    width: "100%",
                  }}
                />
              </div>
              <div
                style={{ flex: 1, display: "flex", flexDirection: "column" }}>
                <div style={{ fontSize: 13, flex: 1, flexGrow: 1 }}>
                  {item.name}
                </div>
                <div style={{ flex: 1 }}>
                  <div style={{ fontSize: 13, marginTop: 8 }}>$33.3</div>
                  <div style={{ display: "flex", gap: 8, marginTop: 5 }}>
                    <div>
                      <img src={Images.Rating} style={{ height: 12 }} />
                    </div>
                    <div style={{ fontSize: 13 }}>({item?.count})</div>
                  </div>
                </div>
                <div
                  className='add-to-crt'
                  onClick={() => handleAddToCart(item)}>
                  ADD TO CART
                </div>
              </div>
            </div>
          );
        })}
      </div>

      {/* <div className='prod-container'>
        {product.map((item) => {
          return (
            <div style={{ background: "#f3f3f3",width:'15%',height:150,}}>

            </div>
          );
        })}
      </div> */}
    </section>
  );
}
