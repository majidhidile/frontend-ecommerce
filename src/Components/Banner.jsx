import React from "react";
import Slider from "react-slick";
import { Images } from "../Utility/Images";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export function Banner() {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    centerMode: true,
    cssEase: "linear",
    variableWidth: true,
  };

  let tempArray = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
  let sliders = tempArray.map(() => {
    return (
      <div>
        <div style={{ position: "relative" }}>
          <div className='absolute-font'>
            From students to senior citizens this web portal of "Products and
            Classifieds” provides it all
          </div>
          <img src={Images.Slide} style={{ height: 250, width: "98%" }} />
        </div>
      </div>
    );
  });
  return (
    <div style={{ marginTop: "2rem" ,marginBottom:'4rem'}}>
      <Slider {...settings}>{sliders}</Slider>
    </div>
  );
}
