import React from "react";
import { Images } from "../Utility/Images";

export  function Info() {
  return (
    <section className='info-container'>
        <div style={{ display: "flex", gap: 10 }}>
          <div>
            <img src={Images.Phone} style={{ height: 15, width: 15 }} />
          </div>
          <p className='info-txt'>+221 33 66 22</p>
        </div>
        <div style={{ display: "flex", gap: 10 }}>
          <div>
            <img src={Images.Message} style={{ height: 15, width: 15 }} />
          </div>
          <p className='info-txt'>support@elextra.io</p>
        </div>

      <div style={{ display: "flex", gap: 10 }}>
        <div>
          <img src={Images.Location} style={{ height: 15, width: 15 }} />
        </div>
        <p className='info-txt'>Locations</p>
      </div>
      <div style={{width:2,background:"white"}}></div>
      <div style={{ display: "flex", gap: 10 }}>
        <p className='info-txt'>$Doller(US)</p>
        <div>
          <img src={Images.DownArrwo} style={{ height: 15, width: 15 }} />
        </div>
      </div>

      <div style={{ display: "flex", gap: 10 }}>
        <p className='info-txt'>ENG</p>
        <div>
          <img src={Images.DownArrwo} style={{ height: 15, width: 15 }} />
        </div>
      </div>
    </section>
  );
}
