import React from 'react'
import { Info,SearchArea,Category, Banner, BestDeals, Classified } from '../Components'
import Subscribe from '../Components/Subscribe'
import Footer from '../Components/Footer'
import '../App.css'

export  function Home() {
  return (
    <>
    <section style={{overflowX:'hidden'}}>
      <Info />
      <section className='tab-container'>
        <SearchArea />
        <Category />
      </section>
      <Banner />
      <BestDeals />
      <Classified />
      <Subscribe />
      <Footer />

      <div style={{ marginTop: "5rem" }}></div>
    </section>
  </>
  )
}
