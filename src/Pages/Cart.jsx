import React from "react";
import { Category, Info, SearchArea } from "../Components";
import { useDispatch, useSelector } from "react-redux";

export default function Cart() {
  const cartItems = useSelector((state) => state.cartList.cartItems);

  const dispatch = useDispatch();

  const handleRemoveFromCart = (itemId, quantity) => {
    dispatch({ type: "REMOVE_FROM_CART", payload: itemId });
    dispatch({
      type: "ADD_QUANTITY_FROM_CART",
      payload: { productId: itemId, quantity },
    });
  };

  const handleIncreaseQuantity = (itemId) => {
    dispatch({ type: "INCREASE_QUANTITY", payload: itemId });
    dispatch({ type: "DECREMENT_COUNT", payload: itemId });
  };

  const handleDecreaseQuantity = (itemId) => {
    dispatch({ type: "DECREASE_QUANTITY", payload: itemId });
    dispatch({ type: "INCREMENT_COUNT", payload: itemId });
  };

  return (
    <>
      <Info />
      <section className='tab-container'>
        <SearchArea />
      </section>
      <div className='cart-container'>
        <div className='cart-head'>Your Shopping Cart</div>
        <div className='cart-line'></div>
        {cartItems.length == 0 && (
          <div className='empty'>Your cart is empty</div>
        )}
        <div>
          {cartItems &&
            cartItems.map((item) => {
              return (
                <>
                  <div className='cart-card'>
                    <div className='cartimg-con'>
                      <img src={item.img} className='cart-img' />
                    </div>
                    <div>
                      <div className="cart-name">{item.name}</div>
                      <div className="cart-quantity">Quantity:{item.quantity}</div>
                      <div className="count-bt-contain">
                        <div
                          className='cont-btn'
                          onClick={() => handleIncreaseQuantity(item.id)}>
                          +
                        </div>
                        <div
                          className='cont-btn'
                          onClick={() => handleDecreaseQuantity(item.id)}>
                          -
                        </div>
                      </div>
                    </div>
                    <div className='remove-contain'>
                      <div
                        className='remove-btn'
                        onClick={() =>
                          handleRemoveFromCart(item.id, item.quantity)
                        }>
                        remove
                      </div>
                    </div>
                  </div>
                  <div className='crt-line'></div>
                </>
              );
            })}
        </div>
      </div>
    </>
  );
}
