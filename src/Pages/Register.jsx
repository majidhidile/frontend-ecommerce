import React, { useState } from "react";
import { Images } from "../Utility/Images";
import { useNavigate } from "react-router-dom";

export function Register() {
  const navigate=useNavigate()


  const [user,setuser]=useState({
    name:'',email:'',password:''
  })

  const [errorState,setErrorState]=useState()
  const setError = (errorMessage) => {
    setErrorState(errorMessage);

    // Remove the error message after 3 seconds
    setTimeout(() => {
      setErrorState(null);
    }, 3000);
  };

  const handleRegister = () => {
    console.log("HERE-->",user)

    // fetch('http://localhost:2121/api/register', {
    fetch('http://52.66.101.53:2121/api/register', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(user),
    })
      .then(response => response.json())
      .then(data => {
        if (data?.userData?.token) {
          // Save the token in local storage
          localStorage.setItem('lilacuser', JSON.stringify(data.userData));
          // Navigate to the home page
          navigate('/');
        }
        else{
          setError(data.error)
        }

      })
      .catch(error => {
        // Handle any errors that occur during the API call
        console.error("ERROR",error);
      });
  };



  return (
    <div className='login-contain'>
      <div className="login-view" >
        <div className='login-logo-container'>
          <img src={Images.Logo2} className='login-logo' />
        </div>
        <div className="login-txt">Sign Up</div>
        <div>
          <div className="d-flex" >
            <input
              type='text'
              placeholder='Enter name'
              className='email-inpt'
              value={user?.name}
              onChange={e => setuser((prev)=>({
                ...prev,
                name:e.target.value
              }))}
            />
          </div>
          <div className="d-flex"  style={{ marginTop: 25 }}>
            <input
              type='text'
              placeholder='Enter email address'
              className='email-inpt'
              value={user?.email}
              onChange={e => setuser((prev)=>({
                ...prev,
                email:e.target.value
              }))}
            />
          </div>
          <div className="d-flex" style={{ marginTop: 25 }}>
            <input
              type='text'
              placeholder='Enter password'
              className='email-inpt'
              value={user?.password}
              onChange={e => setuser((prev)=>({
                ...prev,
                password:e.target.value
              }))}
            />
          </div>
          {errorState && <div className="error">{errorState}</div>}
          <div className="login-btn" onClick={()=>handleRegister()}>REGISTER</div>
          <div className="dont-accont">Already have an account ?<span className="sign-spn" onClick={()=>navigate('/login')}>Sign In</span></div>

        </div>
      </div>
    </div>
  );
}
