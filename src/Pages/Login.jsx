import React, { useState } from "react";
import { Images } from "../Utility/Images";
import { useNavigate } from "react-router-dom";
import firebase from "../Utility/FireBaseConfig";

export function Login() {
  const navigate = useNavigate();
  const [user, setUser] = useState({
    email: "",
    password: "",
  });
  const [errorState,setErrorState]=useState()

  const setError = (errorMessage) => {
    setErrorState(errorMessage);

    // Remove the error message after 3 seconds
    setTimeout(() => {
      setErrorState(null);
    }, 3000);
  };

  const handleLogin = async () => {
    try {
      const response = await fetch("http://52.66.101.53:2121/api/login", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(user),
      });
      const data = await response.json();

      if (data?.userData?.token) {
        // Save the token in local storage
        localStorage.setItem("lilacuser", JSON.stringify(data.userData));
        // Navigate to the home page
        navigate("/");
      }
      else{
        setError(data.error)
        // console.error("HERE ERROR",data);
      }
    } catch (error) {
      setError(error)
    }
  };
  const handleGoogleLogin = () => {
    const provider = new firebase.auth.GoogleAuthProvider();
    firebase
      .auth()
      .signInWithPopup(provider)
      .then(async (result) => {
        // Handle successful Google login
        const user = result.user;
        const { displayName, email, photoURL } = user;
        const idToken = await user.getIdToken();
        let googleQuery = { email: email, token: idToken };

        const requestOptions = {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(googleQuery),
        };
        fetch("http://52.66.101.53:2121/api/googleAuth", requestOptions)
          .then((response) => response.json())
          .then((data) => {

            if (data.userData.token) {
              // Save the token in local storage
              localStorage.setItem("lilacuser", JSON.stringify(data.userData));
              // Navigate to the home page
              navigate("/");
            }
          })
          .catch((error) => {
            console.error(error);
            // Handle any errors that occur during the API call
          });
      })
      .catch((error) => {
        // Handle error
        console.error(error);
      });
  };
  return (
    <div className='login-contain'>
      <div className='login-view'>
        <div className='login-logo-container'>
          <img src={Images.Logo2} className='login-logo' />
        </div>
        <div className='login-txt'>Sign In</div>
        <div>
          <div className='d-flex'>
            <input
              type='text'
              placeholder='Enter email address'
              className='email-inpt'
              value={user?.email}
              onChange={(e) =>
                setUser((prev) => ({
                  ...prev,
                  email: e.target.value,
                }))
              }
            />
          </div>
          <div className='d-flex' style={{ marginTop: 25 }}>
            <input
              type='text'
              placeholder='Enter password'
              className='email-inpt'
              value={user?.password}
              onChange={(e) =>
                setUser((prev) => ({
                  ...prev,
                  password: e.target.value,
                }))
              }
            />
          </div>
         {errorState && <div className="error">{errorState}</div>}
          <div className='login-btn' onClick={() => handleLogin()}>
            LOGIN
          </div>
          <div className='dont-accont'>
            Don't have an account ?
            <span className='sign-spn' onClick={() => navigate("/register")}>
              Sign Up
            </span>
          </div>
          <div className='line'></div>

          <div className='google-contain'>
            <div className='d-flex'>
              <img
                src={Images.Google}
                style={{ width: 25, height: 25, objectFit: "contain" }}
              />
            </div>
            <div className='continue' onClick={() => handleGoogleLogin()}>
              Continue with google
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
