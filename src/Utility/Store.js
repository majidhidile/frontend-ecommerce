import { createStore, combineReducers } from 'redux';
import productListReducer from '../Reducers/ProductList.reducer';
import cartReducer from '../Reducers/CartList.reducer';


// Combine multiple reducers
const rootReducer = combineReducers({
  productList: productListReducer,
  cartList: cartReducer,
});

// Create the Redux store
const store = createStore(rootReducer);

export default store;
