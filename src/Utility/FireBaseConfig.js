import { initializeApp } from 'firebase/app';
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyCpY3gTVmwZJY2Jons6afHZHRVsAq2iBCA",
    authDomain: "lilac-be375.firebaseapp.com",
    projectId: "lilac-be375",
    storageBucket: "lilac-be375.appspot.com",
    messagingSenderId: "224458084080",
    appId: "1:224458084080:web:0b3af5fb45108b1dbafc35",
    measurementId: "G-FHYPR4KL50"
  };

  // initializeApp(firebaseConfig);
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  export default firebase;
