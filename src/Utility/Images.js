import Phone from "../assets/img/phone.svg";
import Message from "../assets/img/message.svg";
import DownArrwo from "../assets/img/downarowfilld.svg";
import Location from "../assets/img/location.svg";
import Logo from "../assets/img/logo.svg";
import DownArrow from "../assets/img/downarrow.svg";
import Search from "../assets/img/search.svg";
import Fav from "../assets/img/fav.svg";
import Cart from "../assets/img/delete.svg";
import Profile from "../assets/img/profile.svg";
import Logo2 from "../assets/img/logo2.png";
import Fintness from "../assets/img/fitnes.svg";
import Rating from "../assets/img/ratings.svg";

import joystcik from "../assets/img/product/joystick.svg";
import Analog from "../assets/img/product/analog.svg";
import Boat from "../assets/img/product/boat.svg";
import AirPod from "../assets/img/product/irpod.svg";
import Levis from "../assets/img/product/levi.svg";
import Lg from "../assets/img/product/lg.svg";
import Mouse from "../assets/img/product/mouse.svg";
import Sony from "../assets/img/product/sony.svg";
import Xray from "../assets/img/product/x-ray.svg";
import Less from "../assets/img/arrow-left.svg";
import Great from "../assets/img/arrow-right.svg";
import Arrow from "../assets/img/arrow.svg";
import Car from "../assets/img/car.svg";
import Dron from "../assets/img/dron.svg";
import Marshal from "../assets/img/marshal.svg";
import Locationn from "../assets/img/locationn.svg";
import Subscription from "../assets/img/subscri.svg";
import Headphone from "../assets/img/headphon.svg";
import GoogleStore from "../assets/img/googlePlay.svg";
import AppStore from "../assets/img/appStore.svg";
import Slide from "../assets/img/Rectangle 9.svg";
import Google from "../assets/img/google.png";

export const Images = {
  Lg,AppStore,GoogleStore,Slide,Google,
  Less,
  Great,
  Arrow,
  Car,
  Marshal,
  Dron,
  Locationn,
  Subscription,
  Mouse,
  Headphone,
  Sony,
  Xray,
  Levis,
  AirPod,
  Boat,
  Analog,
  joystcik,
  Phone,
  Message,
  DownArrwo,
  Location,
  DownArrow,
  Logo,
  Search,
  Fav,
  Cart,
  Profile,
  Logo2,
  Fintness,
  Rating,
};
