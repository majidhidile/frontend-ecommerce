import { Images } from "../Utility/Images";

const initialState = {
  products: [
    {
      id: 1,
      name: "Fitness and activity Tracker",
      count: 65,
      img: Images.Fintness,
    },
    { id: 2, name: "Xbox White Joystick", count: 5, img: Images.joystcik },
    { id: 3, name: "Super Boost Headphones", count: 15, img: Images.Boat },
    { id: 4, name: "X-Ray 2 Square PACK", count: 50, img: Images.Xray },
    { id: 5, name: "AX1326 Automatic Analog", count: 4, img: Images.Analog },

    { id: 6, name: "Ice White Airpods", count: 19, img: Images.AirPod },
    { id: 7, name: "Hazor Mouse Gaming", count: 20, img: Images.Mouse },
    { id: 8, name: "LG 260 L 3 Star Frost", count: 5, img: Images.Lg },
    {
      id: 9,
      name: "Levi's Men's Printed T-Shirt",
      count: 75,
      img: Images.Levis,
    },
    { id: 10, name: "SONY PLAYSTATION 5", count: 53, img: Images.Sony },
  ],
};

const productListReducer = (state = initialState, action) => {
  switch (action.type) {
    case "DECREMENT_COUNT":
      return {
        ...state,
        products: state.products.map((product) =>
          product.id === action.payload
            ? { ...product, count: product.count - 1 }
            : product
        ),
      };
    case "INCREMENT_COUNT":
      return {
        ...state,
        products: state.products.map((product) =>
          product.id === action.payload
            ? { ...product, count: product.count + 1 }
            : product
        ),
      };

    case "ADD_QUANTITY_FROM_CART":
      const { productId, quantity } = action.payload;
      return {
        ...state,
        products: state.products.map((product) =>
          product.id === productId
            ? { ...product, count: product.count + quantity }
            : product
        ),
      };

    default:
      return state;
  }
};

export default productListReducer;
