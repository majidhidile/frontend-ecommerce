const initialState = {
    cartItems: [],
  };

  const cartReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'ADD_TO_CART':
        const newItem = action.payload;
        const existingItem = state.cartItems.find(item => item.id === newItem.id);

        if (existingItem) {
          // If the item already exists in the cart, increase its count
          return {
            ...state,
            cartItems: state.cartItems.map(item =>
              item.id === existingItem.id
                ? { ...item, quantity: item.quantity + 1 } // Increase the quantity by 1
                : item
            ),
          };
        } else {
          // If the item doesn't exist in the cart, add it with a count of 1
          const newItemWithCount = { ...newItem, quantity: 1 };
          return {
            ...state,
            cartItems: [...state.cartItems, newItemWithCount],
          };
        }
      case 'REMOVE_FROM_CART':
        const itemId = action.payload;
        return {
          ...state,
          cartItems: state.cartItems.filter(item => item.id !== itemId),
        };
      case 'INCREASE_QUANTITY':
        const increaseItemId = action.payload;
        return {
          ...state,
          cartItems: state.cartItems.map(item =>
            item.id === increaseItemId ? { ...item, quantity: item.quantity + 1 } : item
          ),
        };
      case 'DECREASE_QUANTITY':
        const decreaseItemId = action.payload;
        return {
          ...state,
          cartItems: state.cartItems.map(item =>
            item.id === decreaseItemId && item.quantity > 1
              ? { ...item, quantity: item.quantity - 1 }
              : item
          ),
        };
      default:
        return state;
    }
  };

  export default cartReducer;
