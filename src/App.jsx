import "./App.css";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  useNavigate,
} from "react-router-dom";

import { Home, Login, Register } from "./Pages";
import { useEffect } from "react";
import Cart from "./Pages/Cart";

function App() {
  const isAuthenticated = true;

  const PrivateRoute = ({ children }) => {
    const navigate = useNavigate();

    useEffect(() => {
      if (!localStorage.getItem("lilacuser")) {
        navigate("/login", { replace: true });
      }
    }, [navigate]);

    return isAuthenticated ? <>{children}</> : null;
  };

  return (
    <>
      <Router>
        <Routes>
          <Route
            path='/'
            element={
              <PrivateRoute>
                <Home />
              </PrivateRoute>
            }
          />
          <Route
            path='/cart'
            element={
              <PrivateRoute>
                <Cart />
              </PrivateRoute>
            }
          />
          <Route path='/login' element={<Login />} />
          <Route path='/register' element={<Register />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
